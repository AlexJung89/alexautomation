import allure
from allure_commons.types import AttachmentType
from Scripts.ScreenObjects.Locators import Locator
from appium.webdriver.common.touch_action import TouchAction
from time import sleep

class HomeSlide(object):

    def __init__(self, driver):
        self.yesiwillbtn = driver.find_element_by_xpath(Locator.yesIWill_btn)
        self.coronatitiletext = driver.find_element_by_xpath(Locator.corona_guidelinetitle_txt)
        self.programslidebar = driver.find_element_by_xpath(Locator.program_slidebar)

    def TapYesIWill(self, yesiwillbtn, coronatitiletext, driver):
        try:
            assert self.coronatitiletext.is_displayed(), "corona message title is not displayed"
            self.yesiwillbtn.click()
        except:
            allure.attach(self.driver.get_screenshot_as_png(), name="Test Corona Message", attachment_type=AttachmentType.PNG)
            pass
        #/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View[3]/android.view.View[2]/android.widget.TextView[15]

    def SlideProgramList(self, driver, programslidebar):
        prog_slid_location=programslidebar.location

        touch = TouchAction(driver)
        for i in range(16):
            touch.press(x=prog_slid_location['x'] + 100, y=prog_slid_location['y'] + 100).move_to(
                x=prog_slid_location['x'] + 600, y=prog_slid_location['y'] + 100).release().perform()

    def SlideDownTotheEnd(self, driver):
        touch = TouchAction(driver)
        for i in range(10):
            touch.long_press(x=500, y=1000).move_to(x=500, y=100).release().perform()

    def SlideUpTotheTop(self, driver):
        touch = TouchAction(driver)
        for i in range(10):
            touch.long_press(x=500, y=100).move_to(x=500, y=1000).release().perform()

class HomeTabs(object):

    def __init__(self,driver):
        self.coronatitiletext = driver.find_element_by_xpath(Locator.corona_guidelinetitle_txt)
        self.yesiwillbtn = driver.find_element_by_xpath(Locator.yesIWill_btn)
        self.onlytab = driver.find_element_by_xpath(Locator.only_tab)
        self.fourtotentab = driver.find_element_by_xpath(Locator.fourtoten_tab)
        self.eighttotentab = driver.find_element_by_xpath(Locator.eighttoten_tab)
        self.eleventothirteentab = driver.find_element_by_xpath(Locator.eleventothirteen_tab)

    def TapYesIWill(self, yesiwillbtn,coronatitiletext,driver):
        assert self.coronatitiletext.is_displayed(), "corona message title is not displayed"
        allure.attach(driver.get_screenshot_as_png(), name="Test Corona Message",
                      attachment_type=AttachmentType.PNG)
        self.yesiwillbtn.click()



    def OnlyTab(self, driver):
        self.onlytab.click()

    def Tap4to7(self, driver):
        self.fourtotentab.click()

    def Tap8to10(self, driver):
        self.eighttotentab.click()

    def Tap11to13(self, driver):
        self.eleventothirteentab.click()

