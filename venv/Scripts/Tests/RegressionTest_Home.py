import unittest
import json
from appium import webdriver
from time import sleep
from appium.webdriver.common.touch_action import TouchAction
from unittest import TestCase, main
from Scripts.ScreenObjects.ParentAPP_Home import HomeSlide, HomeTabs
import allure
import pytest





@allure.story('Regression Test - Alex')
@allure.feature('Test- Automation Frramework with Python')
@allure.testcase('Parent App Regression Test Case 1')


class RegressionTest(unittest.TestCase):

    def setUp(self):
        with allure.step("Launch APP"):
            # office test phone
            with open('C:/Users/Alex/PycharmProjects/PoCAppium/venv/Scripts/Data/Device_MyNote8_uid', encoding='utf-8') as data_file:
                data = json.loads(data_file.read())
            desired_cap = {}
            desired_cap['deviceName'] = data['deviceName']
            desired_cap['udid'] = data['udid']
            desired_cap['platformName'] = data['platformName']
            desired_cap['app'] = data['app']

        self.driver = webdriver.Remote("http://localhost:4723/wd/hub", desired_cap)

    def tearDown(self):
        "Tear down the test"
        self.driver.quit()

    def test_Parent_home_tabs(self):
        "Test the tabs "

        # buffer time for starting the app
        sleep(15)
        self.driver.implicitly_wait(10)

        hometabs = HomeTabs(self.driver)

        # Clicking Corona notification
        with allure.step("Click notification"):
            hometabs.TapYesIWill(hometabs.yesiwillbtn)
            sleep(10)
            self.driver.implicitly_wait(10)

        #checking all the tabs open
        with allure.step("4-7yo tab"):
            hometabs.Tap4to7(self.driver)
            sleep(5)
            self.driver.implicitly_wait(10)

        with allure.step("8-10yo tab"):
            hometabs.Tap8to10(self.driver)
            sleep(5)
            self.driver.implicitly_wait(10)

        with allure.step("11-13yo tab"):
            hometabs.Tap11to13(self.driver)
            sleep(5)
            self.driver.implicitly_wait(10)

        with allure.step("Only tab"):
            hometabs.OnlyTab(self.driver)


    def test_Parent_home_slide(self):
        "Test the app"

        #buffer time for starting the app
        sleep(15)
        self.driver.implicitly_wait(10)

        homeslide = HomeSlide(self.driver)

        # Clicking Corona notification
        with allure.step("Click notification"):
            homeslide.TapYesIWill(homeslide.yesiwillbtn)
            sleep(10)
            self.driver.implicitly_wait(10)


        # Program slide to left
        with allure.step("slid left the program list"):
            homeslide.SlideProgramList(self.driver, homeslide.programslidebar)


        #Slide down to the bttom
        with allure.step("slid down"):
            homeslide.SlideDownTotheEnd(self.driver)

        # Slide back up to the top
        with allure.step("slide up"):
            homeslide.SlideUpTotheTop(self.driver)






#pytest venv\Scripts\Tests\RegressionTest_Home.py --alluredir=C:\allure_report
#allure serve C:\allure_report


if __name__ == '__main__':
    unittest.main()





